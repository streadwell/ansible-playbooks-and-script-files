#!/bin/bash

# Get the current date
date=$(date)

# Get the last 10 users who logged in
last_users=$(last -n 10 | awk '{print $1}' | uniq)

# Get swap space information
swap_space=$(free -m | grep Swap | awk '{print $2 " MB"}')

# Get kernel version
kernel_version=$(uname -r)

# Get IP address
ip_address=$(hostname -I)

# Output gathered information to a file
cat << EOF > /tmp/serverinfo.info
Server Information:
Date: $date

Last 10 Users Who Logged In:
$last_users

Swap Space:
$swap_space

Kernel Version:
$kernel_version

IP Address:
$ip_address
EOF
