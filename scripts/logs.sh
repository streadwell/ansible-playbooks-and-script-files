#!/bin/sh

## date format ##
NOW=$(date +"%F")

tail -n 2000 /var/log/messages >> /lfjs/logs/messages-$NOW
tail -n 2000 /var/log/secure >> /lfjs/logs/secure-$NOW
