
import socket
import platform
import os

def get_ip_address():
    try:
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        return ip_address
    except socket.error:
        return "Unable to get IP address"

def get_vm_info():
    # Hostname
    hostname = socket.gethostname()

    # IP Address
    ip_address = get_ip_address()

    # OS System
    os_system = platform.system()

    # Release of OS
    os_release = platform.release()

    # Version of OS
    os_version = platform.version()

    # Machine architecture
    machine_arch = platform.machine()

    # Display the information
    print(f"Hostname: {hostname}")
    print(f"IP Address: {ip_address}")
    print(f"OS System: {os_system}")
    print(f"Release of OS: {os_release}")
    print(f"Version of OS: {os_version}")
    print(f"Machine Architecture: {machine_arch}")

if __name__ == "__main__":
    get_vm_info()
